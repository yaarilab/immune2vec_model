from dataset import prepare_dataset, split_dataset_folds, split_vectors_folds
from embedding import generate_model, generate_vectors
from feature_engineering import vec_hierarchical_clustering, build_vec_feature_list, build_vec_feature_table, rf_fe
from classification import lg
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import os
import argparse
from enum import Enum, auto


class ModelStep(Enum):
    prepare_dataset = auto()
    generate_model = auto()
    generate_vectors = auto()
    split_dataset_folds = auto()
    split_vectors_folds = auto()
    clustering = auto()
    build_feature_list = auto()
    build_feature_table = auto()
    feature_elimination = auto()
    classify = auto()


current_step = ModelStep.prepare_dataset.value
start_step = ModelStep.prepare_dataset.value
end_step = ModelStep.classify.value


def increment_step():
    global current_step
    global start_step
    global end_step
    ret = True
    if current_step > end_step:
        ret = False
    if current_step < start_step:
        ret = False
    current_step += 1
    return ret


def main():

    parser = argparse.ArgumentParser('Run the immune2vec model on the HCV TCR dataset.')
    parser.add_argument('--start_step',
                        help='First model step, one of: {}, default is "prepare_dataset".'.format(ModelStep.__members__.keys()),
                        type=lambda x: ModelStep.__dict__.get(x).value, default=ModelStep.prepare_dataset.value)
    parser.add_argument('--end_step',
                        help='Last model step, one of: {}, default is "classify".'.format(ModelStep.__members__.keys()),
                        type=lambda x: ModelStep.__dict__.get(x).value, default=ModelStep.classify.value)
    args = parser.parse_args()

    global start_step
    start_step = args.start_step
    global end_step
    end_step = args.end_step

    if increment_step():
        # class PrepareDataArgs:
        #     def __init__(self):
        #         self.data_file_path = "hcv_bcr.tsv"
        #         self.output_desc = "hcv_bcr"
        #         self.output_folder_path = "."
        #         self.is_naive = False
        #         self.min_seq_per_subject = 2000
        #         self.subject_col = 'subject_id'
        #         self.label_col = 'disease_diagnosis'
        # prepare_dataset.prepare_dataset_exec(PrepareDataArgs())
        # this step is skipped in the example as the prepared hcv file is already after this processing

    if increment_step():
        class GenerateModelArgs:
            def __init__(self):
                self.data_file_path = "celiac_igh.tsv"
                self.output_folder_path = '.'
                self.desc = "celiac_igh"
                self.aa_col = 'cdr3_aa'
                self.n_dim = 100
                self.data_fraction = 1.0
                self.seed = 0
                self.n_gram = 3
                self.corpus_file = None
                self.reading_frame = None
        generate_model.generate_model_exec(GenerateModelArgs())

    if increment_step():
        class GenerateVectorsArgs:
            def __init__(self):
                self.data_file_path = "hcv_bcr_prepared.tsv"
                self.output_folder_path = '.'
                self.model_file_path = 'celiac_igh.model'
                self.column = 'cdr3_aa'
                self.n_read_frames = 3
        generate_vectors.generate_vectors_exec(GenerateVectorsArgs())

    if increment_step():
        class SplitFoldsArgs:
            def __init__(self):
                self.data_file_path = "hcv_bcr_prepared_filtered.tsv"
                self.output_desc = 'hcv_bcr'
                self.output_folder_path = 'hcv_bcr_splits'
                self.n_splits = 10
                self.n_repeats = 10
                self.test_size = 0.1  # irrelevant
                self.shuffle_labels = False
                self.stratified = True
                self.shuffle_splits = False
        split_dataset_folds.split_dataset_folds_exec(SplitFoldsArgs())

    if increment_step():
        class SplitVectorsArgs:
            def __init__(self):
                self.data_file_path = "hcv_bcr_prepared_filtered.tsv"
                self.vectors_file_path = "hcv_bcr_prepared_celiac_igh_vectors.npy"
                self.folds_dir = 'hcv_bcr_splits'
                self.data_file_desc = 'hcv_bcr'
        split_vectors_folds.split_vectors_folds_exec(SplitVectorsArgs())

    # aggregate results
    report_list = []
    confusion_mat = None
    for split_id in range(100):

        global current_step
        current_step = ModelStep.clustering.value
        split_folder = os.path.join('hcv_bcr_splits', 'split' + str(split_id))

        if increment_step():
            class VecHierarchicalClusteringArgs:
                def __init__(self):
                    self.train_file_path = os.path.join(split_folder, 'hcv_bcr_train.tsv')
                    self.vectors_file_path = os.path.join(split_folder, 'hcv_bcr_train_vectors.npy')
                    self.num_cpus = None
                    self.thread_memory = None
                    self.linkage = 'complete'
                    self.cluster_col = 'immune2vec_cluster_id'
                    self.cutoff_factor = 0.65
                    self.affinity = 'euclidean'
            vec_hierarchical_clustering.vec_hierarchical_clustering_exec(VecHierarchicalClusteringArgs())

        if increment_step():
            class BuildFeatureListArgs:
                def __init__(self):
                    self.train_file_path = os.path.join(split_folder, 'hcv_bcr_train.tsv')
                    self.vectors_file_path = os.path.join(split_folder, 'hcv_bcr_train_vectors.npy')
                    self.output_desc = 'hcv_bcr_immune2vec'
                    self.output_folder_path = split_folder
                    self.cutoff_factor = 0.65
                    self.subjects_th = 2
                    self.significance_th = 1.5
                    self.cluster_size_th = 10
                    self.target_labels = 'Spontaneously cured from HCV'
                    self.cluster_col = 'immune2vec_cluster_id'
                    self.metric = 'euclidean'
            build_vec_feature_list.build_vec_feature_list_exec(BuildFeatureListArgs())

        if increment_step():
            class BuildFeatureTableArgs:
                def __init__(self):
                    self.train_file_path = os.path.join(split_folder, 'hcv_bcr_train.tsv')
                    self.test_file_path = os.path.join(split_folder, 'hcv_bcr_test.tsv')
                    self.train_vectors_file_path = os.path.join(split_folder, 'hcv_bcr_train_vectors.npy')
                    self.test_vectors_file_path = os.path.join(split_folder, 'hcv_bcr_test_vectors.npy')
                    self.feature_file_path = os.path.join(split_folder, 'hcv_bcr_immune2vec_features.tsv')
                    self.output_folder_path = split_folder
                    self.output_desc = 'hcv_bcr'
                    self.num_cpus = None
                    self.thread_memory = None
                    self.subject_col = 'subject_id'
                    self.metric = 'euclidean'
            build_vec_feature_table.build_vec_feature_table_exec(BuildFeatureTableArgs())

        if increment_step():
            class FeatureEliminationArgs:
                def __init__(self):
                    self.train_features_file = os.path.join(split_folder, 'hcv_bcr_train_features.tsv')
                    self.test_features_file = os.path.join(split_folder, 'hcv_bcr_test_features.tsv')
                    self.output_folder_path = split_folder
                    self.train_labels_file = os.path.join(split_folder, 'hcv_bcr_train_labels.tsv')
                    self.num_features = None
                    self.subject_col = 'subject_id'
            rf_fe.rf_fe_exec(FeatureEliminationArgs())

        if increment_step():
            class ClassifierArgs:
                def __init__(self):
                    self.train_features_file = os.path.join(split_folder, 'hcv_bcr_train_features_rf_fe.tsv')
                    self.test_features_file = os.path.join(split_folder, 'hcv_bcr_test_features_rf_fe.tsv')
                    self.train_labels_file = os.path.join(split_folder, 'hcv_bcr_train_labels.tsv')
                    self.test_labels_file = os.path.join(split_folder, 'hcv_bcr_test_labels.tsv')
                    self.subject_col = 'subject_id'
                    self.label_col = 'disease_diagnosis'
                    self.labels_to_classify = None
                    self.C = 0.003
                    self.l1_ratio = None
                    self.optimize = False
                    self.penalty = lg.Penalty.l2.value
            clf, report, cm = lg.lg_exec(ClassifierArgs())

            if confusion_mat is None:
                confusion_mat = cm
            else:
                confusion_mat += cm
            report_list += [report]

    print('\n***Summary***\n')
    report = pd.concat(report_list, ignore_index=True)
    print(report.mean(axis=0).transpose())
    output_file = os.path.join('classification_report.csv')
    report.to_csv(output_file)
    print('classification report saved to: {}'.format(output_file))

    print(confusion_mat)
    output_file = os.path.join("confusion_matrix.csv")
    confusion_mat.to_csv(output_file)
    print('confusion matrix saved to: {}'.format(output_file))


if __name__ == "__main__":
    main()
